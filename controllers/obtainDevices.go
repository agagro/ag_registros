package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/agagro/ag_registros/models"
	u "bitbucket.org/agagro/ag_registros/utils"

	"github.com/gorilla/mux"
)

var Testtwo = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	fmt.Print(params)
	id, err := strconv.Atoi(params["id"])
	fmt.Println(params)
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	orga, err := strconv.Atoi(params["organization"])
	fmt.Println(params)
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	data := models.Getdevices(uint(orga), uint(id))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}
