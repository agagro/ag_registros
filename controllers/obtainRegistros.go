package controllers

import (
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"bitbucket.org/agagro/ag_registros/models"
	u "bitbucket.org/agagro/ag_registros/utils"

	"github.com/gorilla/mux"
)

var Testthree = func(w http.ResponseWriter, r *http.Request) {

	re := regexp.MustCompile("((19|20)\\d\\d)[-]([0][1-9]|1[012])[-]([0][1-9]|[12][0-9]|3[01])[T]([0][0-9]|[1][0-9]|[2][0-3])(:)([0][0-9]|[1-5][0-9])(:)([0][0-9]|[1-5][0-9])")
	params := mux.Vars(r)
	fmt.Print(params)

	initime := params["initTime"]
	endtime := params["endTime"]
	fmt.Println()
	fmt.Printf("Date: %v :%v\n", initime, re.MatchString(initime))
	fmt.Printf("Date: %v :%v\n", endtime, re.MatchString(endtime))

	if !re.MatchString(initime) || !re.MatchString(endtime) {
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	yearinit, _ := strconv.Atoi(initime[0:4])
	montinit, _ := strconv.Atoi(initime[5:7])
	dayinit, _ := strconv.Atoi(initime[8:10])
	yearend, _ := strconv.Atoi(endtime[0:4])
	montend, _ := strconv.Atoi(endtime[5:7])
	dayend, _ := strconv.Atoi(endtime[8:10])
	fmt.Println(yearinit, montinit, dayinit, yearend, montend, dayend)
	if yearinit != yearend || montinit != montend || dayend-dayinit <= 1
	 && dayend-dayinit >= 0 {
		u.Respond(w, u.Message(false, "NO SE ACEPTA LA FECHA"))
		return

	}

	/*
		AHORA COMPROBAR:
			INITTIME

	*/

	id, err := strconv.Atoi(params["id"])
	fmt.Println(params)
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	device, err := strconv.Atoi(params["device"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	data := models.Getregistros(uint(id), uint(device), initime, endtime)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}
