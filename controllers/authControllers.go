package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/agagro/ag_registros/models"
	u "bitbucket.org/agagro/ag_registros/utils"
)

var CreateAccount = func(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("creando cuenta\n")

	var account *models.Account = new(models.Account)

	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}
	fmt.Println("not serror222\n")
	fmt.Println(account)
	fmt.Println("not serrorEEE\n")

	resp := account.Create() //Create account

	fmt.Println("not serrorFIN\n")
	u.Respond(w, resp)
	fmt.Println("not serrorFIN\n")
}

var Authenticate = func(w http.ResponseWriter, r *http.Request) {

	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account) //decode the request body into struct and failed if any error occur
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := models.Login(account.Email, account.Password)
	u.Respond(w, resp)
}
