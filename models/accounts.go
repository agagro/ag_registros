package models

import (
	"fmt"
	"os"
	"strings"

	u "bitbucket.org/agagro/ag_registros/utils"

	"bitbucket.org/agagro/database"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"golang.org/x/crypto/bcrypt"
)

type Token struct {
	UserId uint
	jwt.StandardClaims
}

//a struct to rep user account
type Account struct {
	gorm.Model
	Email    string `json:"email"`
	Password string `json:"password"`
	Token    string `json:"token";sql:"-"`
}

//Validate incoming user details...
func (account Account) Validate() (map[string]interface{}, bool) {

	fmt.Print("validar\n")
	fmt.Print(account.Email)
	fmt.Print("\n")

	if !strings.Contains(account.Email, "@") {
		return u.Message(false, "Email address is required"), false
	}

	if len(account.Password) < 6 {
		return u.Message(false, "Password is required"), false
	}

	//temp := &Account{}
	var temp Account
	fmt.Print(account.Email)
	fmt.Print("\n")
	fmt.Print(temp)
	fmt.Print("\n")
	fmt.Print("fix query prueba\n")
	database.Runtwo()

	fmt.Print("fix query\n")
	err := database.DBtwo.Table("accounts").Where("email = ?", account.Email).First(&temp).Error

	//err := database.DBtwo.Model(&Account{}).Where("email = ?", ac).First(temp).Error
	fmt.Print("\n")
	fmt.Print(temp)
	fmt.Print("\n")
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry"), false
	}

	fmt.Print("\nhola aca\n")

	fmt.Print("\nhola aca\n")
	if temp.Email != "" {
		return u.Message(false, "Email address already in use by another user."), false
	}
	fmt.Print("\nhola afinca\n")
	return u.Message(false, "Requirement passed"), true
}

func (account Account) Create() map[string]interface{} {
	//var account *Account = new(Account)
	database.Runtwo()
	fmt.Print(account.Email)
	fmt.Print("\nCREAR CUENTA\n")
	if resp, ok := account.Validate(); !ok {
		fmt.Print("hola return\n")
		return resp
	}
	fmt.Print("hola acaYYY\n")
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	account.Password = string(hashedPassword)
	fmt.Println(hashedPassword)

	database.DBtwo.Create(&account)

	if account.ID <= 0 {
		return u.Message(false, "Failed to create account, connection error.")
	}

	//Create new JWT token for the newly registered account
	tk := &Token{UserId: account.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString

	account.Password = "" //delete password

	response := u.Message(true, "Account has been created")
	response["account"] = account
	return response
}

func Login(email, password string) map[string]interface{} {
	database.Runtwo()
	account := &Account{}
	err := database.DBtwo.Table("accounts").Where("email = ?", email).First(account).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return u.Message(false, "Email address not found")
		}
		return u.Message(false, "Connection error. Please retry")
	}

	err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		return u.Message(false, "Invalid login credentials. Please try again")
	}
	//Worked! Logged In
	account.Password = ""

	//Create JWT token
	tk := &Token{UserId: account.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString //Store the token in the response

	resp := u.Message(true, "Logged In")
	resp["account"] = account
	return resp
}

func GetUser(u uint) *Account {
	database.Runtwo()
	acc := &Account{}
	database.DBtwo.Table("accounts").Where("id = ?", u).First(acc)
	if acc.Email == "" { //User not found!
		return nil
	}
	acc.Password = ""
	return acc
}
