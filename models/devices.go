package models

import (
	"fmt"

	"bitbucket.org/agagro/database"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Device struct {
	gorm.Model
	Device_name       string
	Id_organization   int `sql:"type:int REFERENCES clients(id)" `
	Device_autor      string
	Device_tipe       string
	Device_model      string
	Device_version__h string
	Device_chip_imei  string
}

func Getdevices(id_organ uint, id2 uint) []*Device {
	database.Runtwo()

	_, ok, _ := Autorization(id2)
	if !ok {
		return nil
	}

	devices := make([]*Device, 0)

	err := database.DBtwo.Table("devices").Where("id_organization = ?", id_organ).Find(&devices).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return devices
}
