package models

import (
	"fmt"

	u "bitbucket.org/agagro/ag_registros/utils"
	"bitbucket.org/agagro/database"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Organization struct {
	gorm.Model
	Rutempresa           string
	Nombre               string
	Nombre_representante string
	Invoice_address      string
	Invoice_email        string
	Invoice_phone        string
}

type Userapi struct {
	gorm.Model
	Id_comp        int
	Nombre_usuario string
	Email          string
	Invoice_phone  string
	Estado_api     bool
	Passwoord      string
	Token          string
}

//LA ORGANIZACION ID SE RECUPERA CON EL ID DEL USUARIOAPI

func Autorization(userapi uint) (map[string]interface{}, bool, int) {
	database.Runtwo()
	usuario_api := &Userapi{}
	err := database.DBtwo.Table("usuariosapi").Where("id = ?", userapi).First(usuario_api).Error
	if err != nil {
		return u.Message(false, "ERROR IN QUERY"), false, -1
	}
	if !usuario_api.Estado_api { //si es false se debe cambiar y apagar task
		fmt.Println("TASK RUN FALSE POR TANTO CERRAMOS APLICACION ")
		return u.Message(false, "User is not AUTORIZADO"), false, -1
	} else {
		fmt.Println("TASK RUN TRUE POR TANTO SEGUIMOS ESCUCHANDO PAQUETES")
		return u.Message(true, "success"), true, usuario_api.Id_comp
	}

}

func Getorganization(userapi uint) []*Organization {
	database.Runtwo()

	_, ok, id := Autorization(userapi)
	if !ok {
		return nil
	}

	organizations := make([]*Organization, 0)

	err := database.DBtwo.Table("organization").Where("id = ?", id).Find(&organizations).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return organizations
}
