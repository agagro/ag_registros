package models

import (
	"fmt"
	"time"

	"bitbucket.org/agagro/database"
	"github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Registro struct {
	Created_at  time.Time
	Id          int64
	Id_broker   uint
	Timer       postgres.Jsonb
	Metadata    postgres.Jsonb
	Datasensado postgres.Jsonb
}

type Dataoriginal struct {
	Created_at time.Time
	Id         int64
	Id_broker  uint
	Timer      struct {
		Timestamp  time.Time
		Created_at time.Time
	}
	Metadata struct {
		Bateria    float64 `json:"B"`
		Contador   int     `json:"C"`
		Estado     int     `json:"E"`
		ID         int     `json:"Id"`
		Longitud   float64 `json:"LD"`
		Latitud    float64 `json:"LT"`
		MemoriaRam float64 `json:"M"`
		Rssi       float64 `json:"R"`
		Voltaje    float64 `json:"V"`
	}
	Datasensado struct {
		V1 float64 `json:"v1"`
		V2 float64 `json:"v2"`
		V3 float64 `json:"v3"`
		V4 float64 `json:"v4"`
		V5 float64 `json:"v5"`
		V6 float64 `json:"v6"`
		V7 float64 `json:"v7"`
		V8 float64 `json:"v8"`
		V9 float64 `json:"v9"`
	}
}

//LOS REGISTROS SE TOMAN CON EL ID DEL DEVICES Y ADEMAS CON EL LIMITE DE FECHAS.

func Getregistros(id_user uint, device uint, initTime string, endTime string) []*Registro {
	database.Runtwo()

	_, ok, _ := Autorization(id_user)
	if !ok {
		return nil
	}
	fmt.Printf(initTime, endTime, device, id_user)
	registros := make([]*Registro, 0)

	err := database.DBtwo.Table("registros").Where("user_id = ?", device).Find(&registros).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return registros
}
