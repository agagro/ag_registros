package models

import (
	"github.com/jinzhu/gorm"
)

/*
var db *gorm.DB //database

func init() {

	db, err := gorm.Open("postgres", "host=127.0.0.1 port=5432 dbname=gocontacts port=5432 user=postgres password=postgres sslmode=disable")

	if err != nil {
		panic("failed to connect database")
	}

}

*/
var DB *gorm.DB
var db *gorm.DB

func run() {
	var err error
	db, err = gorm.Open("postgres", "host=127.0.0.1 port=5432 dbname=gocontacts port=5432 user=postgres password=postgres sslmode=disable")
	if err != nil {
		panic("failed to connect database")
	}
	//defer db.Close()
	DB = db

}

//returns a handle to the DB object
func GetDB() *gorm.DB {
	return DB
}
