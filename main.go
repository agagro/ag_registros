package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/agagro/ag_registros/app"
	"bitbucket.org/agagro/ag_registros/controllers"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Printf("GELLOP GO SERVER\n")
	router := mux.NewRouter()
	router.Use(app.JwtAuthentication) //attach JWT auth middleware
	router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")
	router.HandleFunc("/api/{id}/contacts", controllers.GetContactsFor).Methods("GET")

	router.HandleFunc("/api/{id}/organization", controllers.Test).Methods("GET")
	router.HandleFunc("/api/{id}/{organization}/devices", controllers.Testtwo).Methods("GET")
	router.HandleFunc("/api/{id}/{device}/registros/{initTime}/{endTime}", controllers.Testthree).Methods("GET")

	router.HandleFunc("/api/contacts", controllers.CreateContact).Methods("POST")
	log.Fatal(http.ListenAndServe(":8001", router))
}

/*
AHORA FALTA RECUPERAR LOS VALORES DEL REGISTROS PARA ELLO DEBEMOS CREAR EL TIPO DE QUERY QUE DESEAMOS

LO MAS LOGICO ES DIVIDIR POR LOS DATOS QUE MANEJA EL USUARIO QUE SON ACONTINUACION
	TOKEN

OBTENER ID DE ORGANIZACION ---->REQUIERE: TOKEN ID_USUARIO
	RETORNA JSON CON [ID_ORGANIZACION,NOMBRE_ORGANIZACION]


OBTENER LISTA DE IDS DE LOS DEVICES--->REQUIERE: TOKEN, ORGANIZACION, id
	RETORNA JSON CON LISTAS DE JSON
	{[
		ID_DEVICE:1,
		NOMBRE:XENON,
		VERSION:1.3.4.2,
	],
	[
		ID_DEVICE:2,
		NOMBRE:XENON,
		VERSION:1.4.3.0,
	]}


OBTENER REGISTRO DE UN DEVICE--->REQUIERE: TOKEN ID_DISPOSITIVO FECHA_INICIO FECHA_TERMINO
	RETORNA JSON CON LISTA DE JSON DE REGISTROS.


/api/{}


*/
